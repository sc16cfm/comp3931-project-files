# This script checks the input and output data folders
# It confirms that each input image (eg NC23SE.png) has an output image of the same name
# If not, it lists the files with no match and can be set to delete them

import os
import glob

# the input and output data folders should be in the same directory
file_path = "/home/cait/university/raw_diss_data/"
extra_folder = ""

B_to_A = False
delete_files = False

if not B_to_A:
    list_1_prefix = "a/" + extra_folder
    list_2_prefix = "b/" + extra_folder

else:
    list_1_prefix = "b/" + extra_folder
    list_2_prefix = "a/" + extra_folder

list_1 = sorted(glob.glob(file_path + list_1_prefix + extra_folder + "*.png"))
list_2 = sorted(glob.glob(file_path + list_2_prefix + extra_folder + "*.png"))

to_delete = []

# keep a list of all files where there isn't a corresponding image in the other folder
for i in range(len(list_1)):
    list_1_file = list_1[i][len(file_path + list_1_prefix + extra_folder):]
    list_2_path = file_path + list_2_prefix + extra_folder + list_1_file

    if list_2_path not in list_2:
        to_delete.append(list_1[i])

# delete files with no counterpart if specified
if len(to_delete) > 0:
    print("Files mismatched: " + str(len(to_delete)))

    for path in to_delete:
        if delete_files:
            print("removing " + path)
            os.remove(path)
        # if not deleting, then print the file name
        else:
            print("File path: " + path)


else:
    print("No mismatched files found")
