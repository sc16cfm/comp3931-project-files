# This script takes:
# - a folder of ASC files coresponding to a UK National Grid tile (NN, NC, etc.)
# - an image of rivers for that National Grid tile
# - an image of peaks for that National Grid tile
# and outputs a formatted image

import numpy as np
import re
import glob
from PIL import Image, ImageFilter

import riverim
import peakim

# tiles: NH, NJ, NN, NO, NC, NY, NT, SE, SX, SH, NR, TF, NM, SO, SN
# done:  NC, NH, NJ, NM, NN, NO, NR, NT, NY, SE, SH, SN, SO, SX, TF

# the name of the National Grid tile files to be formatted
os_tile = 'NC'
file_path = 'path/to/ASC/files/' + os_tile + '/'
river_image = Image.open('./river_lake_grid_img/' + os_tile + '.png')
peak_image = Image.open("./peak_grid_img/" + os_tile + ".png")
save_path = "/path/to/input/data/"

# the river and peak images use coloured grid lines to identify where to crop
# this is normally done by checking the first row and column for these lines
# the SH, NR and NM files instead require the last column to be checked
# when parsing those tiles, use_last_col should be True
use_last_col = False

file_list = sorted(glob.glob(file_path + "*.asc"))

# use river and peak libraries to prepare overlays for water and peaks
river_tiles = riverim.get_tiles_from_im(river_image, os_tile, use_last_col)
peak_tiles = peakim.get_tiles_from_im(peak_image, os_tile, use_last_col)

# for each ASC file in the folder
for id, file in enumerate(file_list):

    f = open(file)
    file_name = file[len(file_path): -4]
    print(file_name)
    dtmlines = f.readlines()
    point_data = dtmlines[6:]
    f.close()

    # make an array based on the number of columns and rows in the ASC grid
    image_cols = int(dtmlines[0].split(" ")[1][:-1])
    image_rows = int(dtmlines[1].split(" ")[1][:-1])
    image_array = np.zeros((image_rows, image_cols))

    n_row = 0
    max_height = -1.0
    min_height = 9999999.9

    # loop to find highest and lowest
    for line in point_data:

        # split the line into an array of numbers
        heightvals = line.split(" ")
        heightvals[-1] = heightvals[-1][:-1]

        # get the max number in the array
        heightvals = [float(i) for i in heightvals]
        line_max = max(heightvals)
        line_min = min(heightvals)

        # check if it's global min/max
        if (line_max > max_height):
            max_height = line_max

        if (line_min < min_height):
            min_height = line_min

    # with max height and min height
    # create thresholds for image height
    height_range = max_height - min_height
    white_thresh = max_height - (height_range * 0.1)
    high_grey_thresh = max_height - (height_range / 3)
    low_grey_thresh = min_height + (height_range / 3)
    black_thresh = min_height + (height_range * 0.1)

    n_row = 0

    # for each line in the file
    for line in point_data:

        n_column = 0

        # split the line into an array of numbers
        heightvals = line.split(" ")
        heightvals[-1] = heightvals[-1][:-1]

        # for each number
        for val in heightvals:

            # get the colour based on the height thresholds
            height = float(val)
            colour = 0

            if height > white_thresh:
                colour = 255
            elif height > high_grey_thresh:
                colour = 192
            elif height > low_grey_thresh:
                colour = 128
            elif height > black_thresh:
                colour = 64
            else:
                colour = 0

            # record our new number in the array
            image_array[n_row][n_column] = colour

            n_column += 1


        n_row += 1


    # so now an array is created
    # so an image is made from the greyscale and the river + peak images
    image = Image.fromarray(image_array).convert('RGBA').resize((256, 256))
    image = image.filter(ImageFilter.MedianFilter(25))
    image.paste(river_tiles[file_name], (0, 0), river_tiles[file_name])
    image.paste(peak_tiles[file_name], (0, 0), peak_tiles[file_name])
    image.save(save_path + file_name + ".png")
