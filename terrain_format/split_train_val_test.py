# pix2pix data needs to be split into train, val and test folders
# so this script takes all the data in the input and output folders,
# splits it randomly,
# and puts both input and output file of the same name in a folder

import os
import shutil
import glob
import random

# script to split entries into train/val/test folders
file_path = "/home/cait/university/raw_diss_data/"
file_list = glob.glob(file_path + "a/*.png")

filename_list = []

# total number of examples = 3206 (/ 3 =  1068.6666)
# values for size of train, val and test
train_total = 2805
val_total = 1590

# make a list of all the file names, randomly shuffle them, and put them in their new home
for file in file_list:
    file_name = file[len(file_path + "a/"):]
    filename_list.append(file_name)

# shuffle the filenames
random.shuffle(filename_list)

file_split = []
train_split = filename_list[:train_total]
val_split = filename_list[train_total:train_total + val_total]
test_split = filename_list[train_total + val_total:]
file_split.append(train_split)
file_split.append(val_split)
file_split.append(test_split)

for id, list in enumerate(file_split):
    folder = ""
    print("id: " + str(id))
    if (id == 0):
        folder = "train/"
    elif (id == 1):
        folder = "val/"
    else:
        folder = "test/"

    for file_name in list:
        source_a = file_path + "a/" + file_name
        source_b = file_path + "b/" + file_name
        dest_a = file_path + "new_a/" + folder + file_name
        dest_b = file_path + "new_b/" + folder + file_name

        shutil.copyfile(source_a, dest_a)
        shutil.copyfile(source_b, dest_b)
