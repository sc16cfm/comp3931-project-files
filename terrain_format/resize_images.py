# resizes all PNG images in a folder to 256x256 for use with pix2pix library

import numpy as np
import re
from PIL import Image
import glob

#file_path = "../database_dtm/"
# full_file_path = "/home/cait/university/raw_diss_data/b/"
full_file_path = "./example_dtm/"
file_list = glob.glob(full_file_path + "*.png")

for file in file_list:
    img = Image.open(file)
    if (img.size[0] != 256 or img.size[1] != 256):
        print(file + " - size: " + str(img.size))
        img = img.resize((256,256))
        img.save(file)
