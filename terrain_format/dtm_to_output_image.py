# This script finds all ASC files in a folder and converts them to PNG images
# the resulting images are proportional terrain height maps

import numpy as np
import re
from PIL import Image
import glob

file_path = "./path/to/ASC/files/"

file_list = sorted(glob.glob(file_path + "*.asc"))
#save_path = "/home/cait/university/raw_diss_data/b/"
save_path = "./path/to/save/images/"

# for each ASC file in the folder
for file in file_list:
    f = open(file)
    file_name = file[len(file_path): -4]
    print(file_name)

    # extract the height data from the ASC file
    dtmlines = f.readlines()
    point_data = dtmlines[6:]
    f.close()

    # make an array based on the number of rows and columns in the ASC file
    image_cols = int(dtmlines[0].split(" ")[1][:-1])
    image_rows = int(dtmlines[1].split(" ")[1][:-1])

    # create an empty array to store values
    image_array = np.zeros((image_rows, image_cols))

    # loop through the values to find the min and max height values
    n_row = 0
    max_height = -1.0
    min_height = 9999999.9

    # loop to find highest and lowest
    for line in point_data:

        # split the line into an array of numbers
        heightvals = line.split(" ")
        heightvals[-1] = heightvals[-1][:-1]

        # get the max number in the array
        heightvals = [float(i) for i in heightvals]
        line_max = max(heightvals)
        line_min = min(heightvals)

        # check if it's global min/max
        if (line_max > max_height):
            max_height = line_max

        if (line_min < min_height):
            min_height = line_min


    # calculate the range of heights in the file from min to max
    # so we can use this to convert heights to grayscale values
    height_range = max_height - min_height

    if (height_range < 1):
        height_range = 1

    increment = 255 / height_range

    # for each line in the file
    for line in point_data:

        n_column = 0

        # split the line into an array of numbers
        heightvals = line.split(" ")
        heightvals[-1] = heightvals[-1][:-1]

        # for each number
        for val in heightvals:

            # assign the colour based on its height value and the range of heights
            height = float(val)
            colour = (height - min_height) * increment

            # record our new number in the array
            image_array[n_row][n_column] = colour

            n_column += 1


        n_row += 1


    # then, convert the array to a grayscale PIL image, resize and save
    image = Image.fromarray(image_array)
    image = image.convert('L')
    image.resize((256, 256))
    image.save(save_path + file_name + ".png")
