# functions for parsing and splitting an image of peaks for a grid tile into 400 transparent images
# which can then be overlaid on a height image
# This requires peak images formatted in a certain way - these are shown in ./peak_grid_img/

import numpy as np
from PIL import Image, ImageChops, ImageFilter

# pass in PIL image, return cropped image by blue grid lines
def crop_by_blue_lines(im, last_col):
    im_array = np.array(im)

    # first, cut the image into a square using the blue lines
    split_im = im.split()
    blue_im = split_im[2] # the blue band of the image

    # now the grid lines show up in white, and everything else is black
    # so I can find the white lines and cut the image based on the line column
    # blue line is about 4-5 pixels thick in the image so I need to record the LAST column of blue (left -> right)

    blue_array = np.asarray(blue_im)
    started_line = False
    ended_line = False
    crop_rows = []

    line_count = 0
    line_mid = 0

    # use the first row to find the column
    first_row = blue_array[0]
    for id, val in enumerate(first_row):
        # if val > 128:
        #     print('BANG! ' + str(id) + " - val " + str(val))

        if val > 128 and not started_line:
            if ended_line:
                line_mid = int(line_count / 2)
                crop_rows.append(id + line_mid)
                break
            else:
                started_line = True
                line_count += 1

        elif val == 0 and started_line:
            ended_line = True
            crop_rows.append(id - line_mid)
            started_line = False

        elif val > 128 and started_line and not ended_line:
            line_count +=1

    started_line = False
    ended_line = False
    crop_cols = []

    if last_col:
        first_col = blue_array[:, -1]
    else:
        first_col = blue_array[:, 0]

    line_count = 0
    line_mid = 0

    for id, val in enumerate(first_col):
        # if val > 128:
        #     ('BANG! ' + str(id) + " - val " + str(val))

        if val > 128 and not started_line:
            if ended_line:
                line_mid = int(line_count / 2)
                crop_cols.append(id + line_mid)
                break
            else:
                started_line = True
                line_count += 1

        elif val == 0 and started_line:
            ended_line = True
            crop_cols.append(id - line_mid)
            started_line = False

        elif val > 128 and started_line and not ended_line:
            line_count +=1


    # I have the crop lines!
    # so, returning to the original image array: im_array
    im_array = im_array[:, crop_rows[0] - 1 : crop_rows[1] + 1]
    im_array = im_array[ crop_cols[0] - 1: crop_cols[1] + 1]
    im = Image.fromarray(im_array)
    print("Cropped image size:")
    print(im.size)

    return im

def make_black_transparent(im):
    # so now we make the black transparent, so just left with red points
    transparency_data = []
    flat_im = im.getdata()
    for val in flat_im:
        if val[0] < 255:
            transparency_data.append((0, 0, 0, 0))
        else:
            transparency_data.append(val)

    im.putdata(transparency_data)
    return im

def break_into_tiles(im, os_tile):
    # now we resize and break it up
    square_im = im.resize((5120, 5120))
    im_array = np.asarray(square_im)
    M = 512
    N = 512
    tiles = []
    for y in range(0, im_array.shape[1], N):
        for x in range(im_array.shape[0] - M, -1, -M):
            # print('x: ' + str(x) + ' y:' + str(y))
            tile_100 = im_array[x:x+M,y:y+N]
            tiles.append(tile_100)

    directions = ['NE', 'NW', 'SE', 'SW']
    tile_quarters = dict()

    M = 256
    N = 256

    for id, tile in enumerate(tiles):
        count = 0
        #im_tile = Image.fromarray(tile)
        #im_tile.save('./rivers/NN' + str(id).zfill(2) + '.png')
        for x in [0, 1]:
            for y in [1, 0]:
                # print('x: ' + str(x) + ' y:' + str(y))
                tile_quarter = tile[x*256:x*256+M,y*256:y*256+N]
                im_tile = Image.fromarray(tile_quarter)
                tile_name = os_tile + str(id).zfill(2) + directions[count]
                tile_quarters[tile_name] = im_tile
                # im_tile.save('./rivers/NN' + str(id).zfill(2) + directions[count] + '.png')
                count += 1
    return tile_quarters

def get_tiles_from_im(im, os_tile, last_col):
    im = crop_by_blue_lines(im, last_col)
    im = make_black_transparent(im)
    tile_quarters = break_into_tiles(im, os_tile)
    return tile_quarters

# def combine_peaks_and_height(height_tile, peak_tile):
#     peak_array = np.array(peak_tile)
#     height_array = np.array(height_tile)
#
#     for i in range(256):
#         for j in range(256):
#             peak_pixel = peak_array[i,j]
#             height_pixel = height_array[i,j]
#
#             # if there's a spot height at this location
#             # check if the pixel value is in the top 30 %: so above 178
#             if peak_pixel[0] > 0 and height_pixel[0] > 178:
#                 # add the red!
#                 height_array[i,j] = (255, 0, 0, 255)
#
#     #print(height_array[0,0])
#     im = Image.fromarray(height_array)
#     return im
