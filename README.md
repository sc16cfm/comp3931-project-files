# COMP3931 Individual Project Repository

This is the repository for files relating to my final project. Some files can't be shared for licensing reasons, but all files that can be shared are available here.

This project required the use of the pix2pix PyTorch framework, both to train the model and load it for the Flask interface. The source code for this framework is available at https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/.
