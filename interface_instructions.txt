To build and launch the interface:

First, the necessary Python packages must be installed; see requirements.txt for a list.
Miniconda was used for Python environments, but all can be installed using pip.

Once the packages are installed, go to /interface/flask_app/ and run

'python app.py'

If you see:
'
model [Pix2PixModel] was created
loading the model from ./checkpoints/terrain_pix2pix/latest_net_G.pth
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
'

The app has loaded successfully. From there, you can navigate to http://127.0.0.1:5000/
and use the interface.

Common issues:

- A CUDA out of memory issue is shown in the terminal when launching the app
The pix2pix model uses torch GPU; a sufficient graphics card is needed. Otherwise, his can happen occasionally,
so just try launching the app again.

- Clicked 'Convert Terrain' and the page reloaded, but the output isn't there
The time for the model to convert the image varies; sometimes the page refreshes before the output is saved.
Check in the terminal for 'output ready' then refresh the page, and the output image will be there.
