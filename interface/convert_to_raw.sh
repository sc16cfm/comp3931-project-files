# This shell script uses the imagemagick library to convert the interface output png to a 16-bit raw file
# the name of the output raw file must be specified as an argument

# This script requires a Linux operating system on which imagemagick is installed
# visit https://imagemagick.org/script/download.php for more information

stream -map r -storage-type short ./flask_app/static/success.png $1.raw
