This folder contains the .pth file which stores the weights of the trained terrain generator model.

In order for the pix2pix loader to find the code, the .pth file is stored under /checkpoints/terrain_pix2pix. Please note
that moving this file will require changes in the app.py code.
