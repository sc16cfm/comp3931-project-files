# Python script that launches a Flask app that serves a webpages and loads the trained model
# When the 'Convert Image' button is clicked on, this code converts the image and saves it for display

# Running this script requires some Python modules to be installed
# See the repository requirements.txt file for more information

from flask import Flask, request, render_template, redirect
from werkzeug.datastructures import ImmutableMultiDict

from pix2pix.options.test_options import TestOptions
from pix2pix.data import CustomDatasetDataLoader
from pix2pix.models import create_model
import pix2pix.util.util as util

from PIL import Image
from io import BytesIO
import re
import os
import base64
import shutil

#--name terrain_pix2pix --model pix2pix --netG unet_256 --direction AtoB --dataset_mode single

# set the options for creating the model and data loader
opt = TestOptions().parse()
opt.model = 'pix2pix'
opt.name = 'terrain_pix2pix'
opt.netG = 'unet_256'
opt.direction = 'AtoB'
opt.dataset_mode = 'single'
opt.nThreads = 1
opt.batchSize = 1
opt.serial_batches = True
opt.no_flip = True
opt.how_many = 1

# create and setup the model with the specified options
model = create_model(opt)
model.setup(opt)
save_path = ''

# launch the Flask app
app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

# define get and post functions for the web page
@app.route('/', methods=['GET', 'POST'])
def app_main():
    global save_path
    if request.method == 'GET':
        print('save_path = ' + save_path)
        return render_template('index.html', output_path=save_path)
    if request.method == 'POST':
        # decode the posted canvas and load it as an image
        data = request.get_data().decode('utf-8')
        image_data = re.sub('data:image/png;base64,', '', data)
        missing_pad = len(image_data) % 4
        if missing_pad > 0:
            image_data += ('=' * (4 - missing_pad))
        image_bytes = base64.b64decode(image_data.encode())
        im = Image.open(BytesIO(image_bytes))

        # resize from 512x512 to 256x256 for use with the model
        im = im.resize((256, 256))

        # save the input image and load it with the pix2pix data module
        # then generate the output image with model.test()
        im.save('./input_img/input.png')
        data_loader = CustomDatasetDataLoader(opt)
        dataset = data_loader.load_data()
        print('Input data loaded')
        for i, data in enumerate(dataset):
            if i >= opt.how_many:
                break
            model.set_input(data)
            # Forward
            model.test()

        # convert output tensor to image and save in static folder
        # so Flask can load it on the web page
        img_array = util.tensor2im(model.fake_B.data)
        save_path = './static/success.png'
        util.save_image(img_array, save_path)
        print('Output ready')

        # refresh the page to display the output image
        return redirect("/")

if __name__ == '__main__':
    app.run(host='127.0.0.1', debug=False)
