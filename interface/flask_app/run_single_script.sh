# this shell script launches the python file that converts a pre drawn image and shows it as a plot
# to convert your image, put it in the /input_img folder (NOTE: it should be the only file in the folder, remove other files)

python generate_output_no_interface.py --dataroot ./input_img --name terrain_pix2pix --model pix2pix --netG unet_256 --direction AtoB --dataset_mode single
