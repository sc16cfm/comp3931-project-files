# Python script that takes a single image and converts it using modified pix2pix libraries
# then displays the result using matplotlib

# To use, place the image you want to convert in the ./input_img/ folder (NOTE: it should be the only image in the folder)
# then run this command:
# python generate_output_no_interface.py --dataroot ./input_img --name terrain_pix2pix --model pix2pix --netG unet_256 --direction AtoB --dataset_mode single

# This python script has some required modules that need to be installed
# See requirements.txt in the repository for more information

import os

from pix2pix.options.test_options import TestOptions
from pix2pix.data import CustomDatasetDataLoader
from pix2pix.models import create_model
import pix2pix.util.util as util

import matplotlib.pyplot as plt

# set up the options for pix2pix to load the model
opt = TestOptions().parse()
opt.model = 'pix2pix'
opt.name = 'terrain_pix2pix'
opt.netG = 'unet_256'
opt.direction = 'AtoB'
opt.dataset_mode = 'single'
opt.nThreads = 1
opt.batchSize = 1
opt.serial_batches = True
opt.no_flip = True
opt.how_many = 1

# create the pix2pix model with the given options
model = create_model(opt)
model.setup(opt)

# load the image from the ./input_img/ folder
data_loader = CustomDatasetDataLoader(opt)
dataset = data_loader.load_data()

for i, data in enumerate(dataset):
    if i >= opt.how_many:
        break
    # send the input image to the torch device
    model.set_input(data)
    # Get the output by forwarding the model through model.test
    # using model.test instead of model.forward sets torch.no_grad
    # this uses less memory
    model.test()

# convert the output tensor to image array using pix2pix util module
img_array = util.tensor2im(model.fake_B.data)

# display the image with a matplotlib plot
imgplt = plt.imshow(img_array)
plt.show()
