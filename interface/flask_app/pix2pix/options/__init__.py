# This is a file taken from the pix2pix framework for image translation with conditional GANs
# The code written belongs to Jun-Yan Zhu and is available on GitHub at https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix
# If any modifications have been made, they are noted with a comment - search for 'NOTE:'

"""This package options includes option modules: training options, test options, and basic options (used in both training and test)."""
