// access the canvas
var canvas = document.getElementById('input_canvas');
var previous_canvas = "";
var context = canvas.getContext('2d');
var sessionStorage = window.sessionStorage;

context.fillStyle = '#787878';
context.fillRect(0, 0, canvas.width, canvas.height);

var dataURL = canvas.toDataURL();
convertIm();

if (sessionStorage.dataURL) {
  dataURL = sessionStorage.dataURL;
  var im = new Image();
  im.src=dataURL;
  im.onload=function(){
    context.drawImage(im,0,0);
  }
}

context.lineWidth = 40;
context.lineCap = "round";
context.strokeStyle = "#a6a6a6";



document.addEventListener("mousemove", draw);
document.addEventListener("mousedown", setPosition);
document.addEventListener("mouseenter", setPosition);


// last known position
var pos = { x: 0, y: 0 };


// set up colour slider
var slider = document.getElementById('slider');
var slider_output = document.getElementById('gray_color');
var hex = "#000000"

slider_output.style.backgroundColor = hex;
slider_output.style.color = "#ffffff";

slider.addEventListener('change', function() {

  getGrayColour();
  slider_output.style.backgroundColor = hex;
  setCanvasBrush(hex, 40);

}, false);


function getGrayColour() {
  var gray = slider.value;
  if (gray > 128) {
    slider_output.style.color = "black";
  }
  else {
    slider_output.style.color = "white";
  }

  var slide_hex = parseInt(gray, 10).toString(16);
  if (slide_hex.length == 1) {
    slide_hex = "0" + slide_hex;
  }

  hex = "#" + slide_hex + slide_hex + slide_hex;
}

function setCanvasBrush(colour, width) {
  context.strokeStyle = colour;
  context.lineWidth = width;
}

// new position from mouse events
function setPosition(e) {
  var rect = canvas.getBoundingClientRect();
  pos.x = e.clientX - rect.left;
  pos.y = e.clientY - rect.top;
}

function draw(e) {
  if (e.buttons !== 1) return; // if mouse is pressed.....

  // var color = document.getElementById("hex").value;

  context.beginPath(); // begin the drawing path

  context.moveTo(pos.x, pos.y);
  setPosition(e);
  context.lineTo(pos.x, pos.y);

  context.stroke();

  convertIm();
}

function convertIm() {
  dataURL = canvas.toDataURL();
}

function binary_to_string(array) {
  var url_string = "";
  for (var i = 0; i < array.length; i++) {
    url_string += String.fromCharCode(parseInt(array[i], 2));
  }
  return url_string;
}

function changeColour(button) {
  var colour = button.value;

  if (colour == "blue") {
    setCanvasBrush("#0000ff", 16);
  }
  if (colour == "red") {
    setCanvasBrush("#ff0000", 10);
  }
}

function clearCanvas() {
  context.fillStyle = '#787878';
  context.fillRect(0, 0, canvas.width, canvas.height);
  convertIm();
}


function sendToFlask() {
  document.getElementById('convert_button').disabled = true;
  sessionStorage.dataURL = dataURL;

  $.ajax({
    type: "POST",
    url: "http://127.0.0.1:5000/",
    data: dataURL,
    success: function(response){
      console.log('Sent image OK')
    }
  })

  window.setTimeout(function(){
    window.location.href = "http://127.0.0.1:5000/";
  }, 4000);
}
