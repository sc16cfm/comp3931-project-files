#!/bin/bash
#$ -cwd
#$ -V
#$ -l coproc_p100=1
#$ -l h_rt=02:00:00
#$ -m be

__conda_setup="$('/nobackup/sc16cfm/miniconda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/nobackup/sc16cfm/miniconda/etc/profile.d/conda.sh" ]; then
        . "/nobackup/sc16cfm/miniconda/etc/profile.d/conda.sh"
    else
        export PATH="/nobackup/sc16cfm/miniconda/bin:$PATH"
    fi
fi
unset __conda_setup

set -ex
module load cuda
conda init
conda activate trainenv

python test.py --dataroot ../dataset --name terrain_pix2pix --model pix2pix --netG unet_256 --direction AtoB --dataset_mode aligned --norm batch
