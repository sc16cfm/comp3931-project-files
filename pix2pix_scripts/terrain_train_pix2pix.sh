#!/bin/bash
#$ -cwd
#$ -V
#$ -l coproc_p100=1
#$ -l h_rt=6:00:00
#$ -m be

__conda_setup="$('/nobackup/sc16cfm/miniconda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/nobackup/sc16cfm/miniconda/etc/profile.d/conda.sh" ]; then
        . "/nobackup/sc16cfm/miniconda/etc/profile.d/conda.sh"
    else
        export PATH="/nobackup/sc16cfm/miniconda/bin:$PATH"
    fi
fi
unset __conda_setup
set -ex

module load cuda
conda init
conda activate trainenv

python ./train.py --dataroot ../dataset --name terrain_pix2pix --model pix2pix --direction AtoB --norm batch --netG unet_256 --display_id 0 --save_epoch_freq 10 --n_epochs 50 --n_epochs_decay 50
